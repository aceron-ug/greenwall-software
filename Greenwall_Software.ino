/*
 * Copyright (C) 2022  ACERON UG (haftungsbeschränkt)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define SENSOR_IN A0
#define PUMP_IO 5

#define THRESHOLD 20                                                //Prozent

void setup() {
    
    pinMode(SENSOR_IN, INPUT);
    pinMode(PUMP_IO, OUTPUT);
    digitalWrite(PUMP_IO, LOW);

    Serial.begin(9600);
}

void loop() {
    int raw;
    int val = map(raw = analogRead(SENSOR_IN), 423, 300, 0, 100);     // (Trocken, Nass, 0%, 100%)
  
    Serial.println(" ");
    Serial.print("Gemessener Wert: ");
    Serial.print(val);
    Serial.print("% (");
    Serial.print(raw);
    Serial.println(")");
    
    if (val <= THRESHOLD) {

        Serial.println("Pumpe wird fuer 5 Sekunden eingeschaltet!");
        
        //digitalWrite(PUMP_IO, HIGH);
        analogWrite(PUMP_IO, 40);                                     // (pwm) 24V netzteil, 90 entspricht etwa 9V output
        delay(5 * 1000);                                              // Pumpzeit
        //digitalWrite(PUMP_IO, LOW);
        analogWrite(PUMP_IO, 0);

        Serial.println("Pumpe ist fertig.");
        
    }

    delay(5000);
    
    //delay(250);
}
